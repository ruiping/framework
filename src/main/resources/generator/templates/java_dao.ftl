package ${pknDao};

import ${pknEntity}.${className};
import ${pknExample}.${className}Example;
import com.framework.common.BaseDao;
import com.framework.common.MyBatisRepository;

@MyBatisRepository
public interface ${className}Dao extends BaseDao<${className}, ${className}Example, Integer>{
   
}