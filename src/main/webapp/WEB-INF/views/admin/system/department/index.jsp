<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}/" />
<!DOCTYPE html>
<html>
<head>
<meta name="department" content="width=device-width" />
<title>部门管理</title>
<!--框架必需start-->
<link href="${ctx }static/css/framework.css" rel="stylesheet">
<script src="${ctx }static/js/jquery-1.8.2.min.js"></script>
<script src="${ctx }static/js/framework.js"></script>
<!--框架必需end-->
<!--jqgrid表格组件start-->
<script src="${ctx }static/js/jqgrid/jquery-ui-custom.min.js"></script>
<script src="${ctx }static/js/jqgrid/grid.local-cn.js"></script>
<link href="${ctx }static/js/jqgrid/css/jqgrid.css" rel="stylesheet" />
<script src="${ctx }static/js/jqgrid/jqGrid.js"></script>
<!--布局组件start-->
<script src="${ctx }static/js/layout/splitter.js"></script>
<!--布局组件end-->
<!--表单验证组件start-->
<script src="${ctx }static/js/validator/framework-validator.js"></script>
<!--表单验证组件end-->
</head>
<body>
	<div>
<script type="text/javascript">
    $(function () {
        getGrid();
    })
    //加载表格
    function getGrid() {
        $("#gridTable").jqGrid({
            url: "${ctx}admin/system/department/list",
            datatype: "json",
            mtype: "post",
            postData: {},
            height: $(window).height() - 149,
            autowidth: true,
            colModel: [
					{ label: '主键', name: 'id', index: "id", hidden: true },
					{ label: '编码', name: 'code', index: "code", width: 80 },
					{ label: '名称', name: 'fullName', index: "fullName", width: 100 },
					{ label: '简称', name: 'shortName', index: "shortName", width: 100 },
					{ label: '电话', name: 'phone', index: "phone", width: 100, align: "center" },
					{ label: '传真', name: 'fax', index: "fax", width: 100, align: "center" },
                    {
                        label: '有效', name: 'enabled', index: 'enabled', width: 45, align: 'center',
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue) {
                            	return "<img src='${ctx}static/images/checkokmark.gif'/>";
                            }else {
                            	return "<img src='${ctx}static/images/checknomark.gif'/>";
                            }
                        }
                    },
                    { label: '说明', name: 'description', index: "description", width: 300 },
                    {
                        label: '创建时间', name: 'createTime', index: 'createTime', width: 0, align: 'left', sortable: true, formatter: function (cellvalue, options, rowObject) {
                        	return cellvalue;
                        }
                    },
                    { label: '创建用户', name: 'createUserName', index: 'createUserName', width: 0, align: 'left', sortable: true }
                    
            ],
            pager: false,
            rowNum: 1000,
            sortname: 'sort',
            sortorder: 'asc',
            rownumbers: true,
            shrinkToFit: false,
            gridview: true
        });
    }
</script>
		<!--工具栏-->
		<div class="tools_bar leftline rightline"
			style="margin: 1px; margin-bottom: 0px;">
			<div class="PartialButton">
			</div>
		</div>
		<div class="leftline rightline QueryArea"
			style="margin: 1px; margin-top: 0px; margin-bottom: 0px;">
			<table border="0" class="form-find" style="height: 45px;">
				<tr>
					<th>关键字：</th>
					<td><input id="keywords" type="text" class="txt"
						style="width: 200px" /></td>
					<td><input id="btnSearch" type="button" class="btnSearch"
						value="搜 索" onclick="btn_Search()" /></td>
				</tr>
			</table>
		</div>
		
		<div class="topline rightline" style="margin: 1px; margin-top: -1px;">
			<table id="gridTable"></table>
			<div id="gridPager"></div>
		</div>

	</div>
</body>
</html>
