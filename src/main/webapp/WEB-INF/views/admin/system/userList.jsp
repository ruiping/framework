<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}/" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>小说站后台管理系统-用户管理</title>
<script type="text/javascript">
        $(document).ready(function () {
            var url = "${ctx}admin/user/list";
            // prepare the data
           	var source =
            {
           		type: 'post',
                datatype: "json",
                datafields: [
                    { name: 'userName', type: 'string' },
                    { name: 'realName', type: 'string' },
                    { name: 'gender', type: 'int' },
                    { name: 'mobile', type: 'string' },
                    { name: 'email', type: 'string' },
                    { name: 'enabled', type: 'int' },
                    { name: 'department.name', type: 'string', map: 'department>name' },
                    { name: 'logOnCount', type: 'int' },
                    { name: 'lastVisit', type: 'date' },
                    { name: 'description', type: 'string' }
                ],
                filter: function(filters, recordsArray){
                	//alert(JSON.stringify(filters));
                	$('#jqxgrid').jqxGrid('updatebounddata');
                },
                sort: function(filters, recordsArray){
                	//alert(JSON.stringify(filters));
                	$("#jqxgrid").jqxGrid('updatebounddata', 'sort');
                },
                id: 'uid',
                url: url,
                root: 'data'
            };
            
            var rendergridrows = function (params) {
            	var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
                return params.data;
            }
            
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(
            {
            	width : '99.8%',
				height : $(window).height() - 99,
                source: dataAdapter,
                altrows: true,//开启交替行背景色
                sortable: true,//开启排序
                showsortmenuitems: false,
                pageable: true,
                pagesize: 20,
                virtualmode: true,//虚拟分页 
                selectionmode: 'multiplerowsextended',//选中多行
                rendergridrows: rendergridrows,
                //pagermode: 'simple',
                //showemptyrow: false,//空数据时不显示
                localization: {
                	pagergotopagestring: "第",
                	pagershowrowsstring: "页  每页显示",
                	pagerrangestring: "共",
                	pagerpreviousbuttonstring : "上一页",
					pagernextbuttonstring : "下一页",
					pagerfirstbuttonstring : "第一页",
					pagerlastbuttonstring : "最后一页",
                	loadtext: "加载中...",
                	emptydatastring: "没有找到符合条件的数据"
                	
                },
                columnsresize: true,
                columnsreorder: true,
                columns: [
                     {
                         text: '', sortable: false, filterable: false, editable: false,
                         groupable: false, draggable: false, resizable: false,
                         datafield: '', columntype: 'number', width: '4%',
                         cellsrenderer: function (row, column, value) {
                             return "<div style='width:50px;text-align:center;margin-top:6px'>" + (value + 1) + "</div>";
                         }
                     },
                     { text: '登陆账号', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'userName', width: '10%'},
                     { text: '姓名', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'realName', width: '10%' },
                     { text: '性别', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'gender', width: '4%', 
                    	 cellsrenderer: function (row, column, value, defaulthtml, columnproperties) {
                    		var newhtml = $(defaulthtml);
                    		return $(defaulthtml).html(value?"男":"女")[0].outerHTML;
                         }
                     },
                     { text: '手机号码', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'mobile', width: '10%' },
                     { text: '电子邮箱', columngroup: 'baseInfo', cellsalign: 'center', align: 'center', datafield: 'email', width: '12%' },
                     { text: '有效', datafield: 'enabled', cellsalign: 'center', align: 'center', threestatecheckbox: true, columntype: 'checkbox', width: '4%' },
                     { text: '部门', cellsalign: 'center', align: 'center', datafield: 'department.name', width: '8%' },
                     { text: '登陆次数', filterable:false, cellsalign: 'center', align: 'center', datafield: 'logOnCount', width: '6%' },
                     { text: '最后登陆时间', cellsalign: 'center', align: 'center', datafield: 'lastVisit', cellsformat: 'yyyy-MM-dd HH-mm-ss', width: '12%'},
                     { text: '说明', cellsalign: 'center', align: 'center', datafield: 'description', width: '20%' }
                ],
   	            columngroups: [
   	              { text: '基本信息', align: 'center', name: 'baseInfo' }
   	            ]
            });
            
            initSearchBar();
       });
        
        //初始化查询工具栏
        function initSearchBar(){
        	//初始化类型下拉框
            var searchTypeSource = [
	                {label: "登陆账号", value: "userName"}, 
	                {label: "姓名", value: "realName"},
	                {label: "手机号码", value: "mobile"},
	                {label: "电子邮箱", value: "email"},
	                {label: "说明", value: "description"}
                ];
            $("#searchType").jqxDropDownList({ 
            	source: searchTypeSource, 
            	width: '80', 
            	height: '25',
            	displayMember: "label", 
            	valueMember: "value",
            	dropDownHeight: 100,
            	selectedIndex: 0
            });
            
            $("#searchType").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {
                    	$("#jqxgrid").jqxGrid('clearfilters');
                    	$("#searchContent").val("");
                    	$("#enabledCheckbox").jqxCheckBox("indeterminate");
                        var valueelement = $("<div></div>");
                        valueelement.text("Value: " + item.value);
                        var labelelement = $("<div></div>");
                        labelelement.text("Label: " + item.label);
                        //alert(valueelement);
                    }
                }
            });
            
            //初始化searchContentInput
            $("#searchContent").jqxInput({
            	placeHolder: "输入关键字", 
            	height: 25, 
            	width: 150,
            	minLength: 1
            })
            
            //初始化查询按钮
            $("#searchButton").jqxButton({
            	width: 60, 
            	height: 18
            });
          	//初始化清空按钮
            $("#clearButton").jqxButton({
            	width: 60, 
            	height: 18
            });
            
            $("#clearButton").on('click', function () {
            	$("#jqxgrid").jqxGrid('clearfilters');
            	$("#searchContent").val("");
            	$("#enabledCheckbox").jqxCheckBox("indeterminate");
            });
            
            $("#searchButton").on('click', function () {
                //获取查询类型
                var searchType = $("#searchType").jqxDropDownList('getSelectedItem');
                //获取关键字
                var searchContent = $("#searchContent").val();
                //如果关键字为空则不创建filter
                if($.trim(searchContent).length > 0) {
                	addFilter("jqxgrid", searchContent, JqxFilter.CONTAINS, JqxFilter.stringfilter, searchType.value);
                }
                //有效性
                var enabled = $("#enabledCheckbox").val();
                if(enabled != null) {
                	addFilter("jqxgrid", enabled, JqxFilter.EQUAL, JqxFilter.booleanfilter, "enabled");
                }
                //应用filters
                $("#jqxgrid").jqxGrid('applyfilters');
            })
            
            //初始化有效性
            $("#enabledCheckbox").jqxCheckBox({ 
            	width: 48, 
            	height: 25,
            	hasThreeStates: true, 
            	checked: null
            });
            
            $('#enabledCheckbox').on('change', function (event) { 
            	var enabled = $(this).val();
            	if(enabled != null) {
                	addFilter("jqxgrid", enabled, JqxFilter.EQUAL, JqxFilter.booleanfilter, "enabled");
                	//应用filters
                    $("#jqxgrid").jqxGrid('applyfilters');
                }else {
                	$("#jqxgrid").jqxGrid('removefilter', "enabled");
                }
            }); 

        }
    </script>
</head>
<body>
	<form method="post" action="UserList.aspx" id="form1">
		<div class="tools_bar">
			<a title="刷新当前页面" onclick="Replace();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/arrow_refresh.png') 50% 4px no-repeat;">刷新</b></span></a>
			<div class="tools_separator"></div>
			<a onclick="add();" class="tools_btn">
				<span><b style="background: url('${ctx}static/images/16/add.png') 50% 4px no-repeat;">新增</b></span>
			</a>
			<a onclick="update();" class="tools_btn">
				<span><b style="background: url('${ctx}static/images/16/edit.gif') 50% 4px no-repeat;">编辑</b></span>
			</a>
			<a onclick="del();" class="tools_btn">
				<span><b style="background: url('${ctx}static/images/16/DeleteRed.png') 50% 4px no-repeat;">删除</b></span>
			</a>
			<div class="tools_separator"></div>
			<a title="查看相关详细信息" onclick="lookup();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/page_white_find.png') 50% 4px no-repeat;">查看详细</b></span></a><a
				title="重新设置新密码" onclick="SetNewPassword();;" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/group_key.png') 50% 4px no-repeat;">重置密码</b></span></a><a
				title="用户分配角色" onclick="AllotRole();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/allot_role.png') 50% 4px no-repeat;">分配角色</b></span></a><a
				title="Excel导入" onclick="import();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/enter.png') 50% 4px no-repeat;">引入</b></span></a><a
				title="导出" onclick="derive();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/out.png') 50% 4px no-repeat;">引出</b></span></a>
			<div class="tools_separator"></div>
			<a title="关闭当前窗口" onclick="ThisCloseTab();" class="tools_btn"><span><b
					style="background: url('${ctx}static/images/16/back.png') 50% 4px no-repeat;">离开</b></span></a>
		</div>
		<div class="btnbarcontetn" style="margin-top: 1px; background: #F7F7F7; border:#ccc 1px solid">
			<div>
				<table border="0" style="height: 45px;background: #F7F7F7;">
					<tr>
						<th style="padding-left:30px">查询条件：</th>
						<td><div id='searchType'></div></td>
						<td><input type="text" id="searchContent"/></td>
						<td><div id="searchButton" style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='${ctx }static/images/16/magnifier.png'/><span style='margin-left: 10px; position: relative; top: -5px;'>搜索</span></div></td>
						<td><div id="clearButton" style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='${ctx }static/images/16/trash_16x16.gif'/><span style='margin-left: 10px; position: relative; top: -5px;'>清空</span></div></td>
						<td><div id='enabledCheckbox' style='margin-left:10px;margin-top:10px;font-family: Verdana Arial;font-size: 12px;float: left;'><span style="padding:2px;">有效性</span></div></td>
					</tr>
				</table>
			</div>
		</div>  
		<div id="jqxgrid" style="margin-top: 1px;"></div>
	</form>
</body>