<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}/"/>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
<title>小说站后台管理系统-登陆页面</title>
<link type="text/css" href="${ctx }static/css/desktop.css" rel="stylesheet">
<link type="text/css" href="${ctx }static/css/login.css" rel="stylesheet">
<script type="text/javascript" src="${ctx }static/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="${ctx }static/js/common.js"></script>
<script type="text/javascript">
		if (top != window){
		  top.location.href = window.location.href;
		}
		
        //回车键
        document.onkeydown = function (e) {
            if (!e) e = window.event; //火狐中是 window.event
            if ((e.keyCode || e.which) == 13) {
                var obtnSearch = document.getElementById("Log_Submit")
                obtnSearch.focus(); //让另一个控件获得焦点就等于让文本输入框失去焦点
                obtnSearch.click();
            }
        }
        
        function LoginBtn() {
            var username = $("#username").val();
            var password = $("#password").val();
            var captcha = $("#captcha").val();
            if ($.trim(username).length == 0) {
                $("#username").focus();
                showTopMsg("登录账户不能为空", 4000, 'error');
                return false;
            } else if ($.trim(password).length == 0) {
                $("#password").focus();
                showTopMsg("登录密码不能为空", 4000, 'error');
                return false;
            } else if ($.trim(captcha).length == 0) {
                $("#captcha").focus();
                showTopMsg("验证码不能为空", 4000, 'error');
                return false;
            } else if ($.trim(captcha).length != 4) {
                $("#captcha").focus();
                showTopMsg("验证码必须为4位", 4000, 'error');
                return false;
            } else {
                return true;
            }
        }
        /** 
         * 数据验证完整性
         */
        function CheckUserDataValid() {
            if (!LoginBtn()) {
                return false;
            }
            else {
                CheckingLogin(1);
                var username = $("#username").val();
                var password = $("#password").val();
                var captcha = $("#captcha").val();
                ajaxSubmit('${ctx}admin/login',{
                	username:username,
                	password:password,
                	captcha:captcha
                },function(data){
                	var code = parseInt(data.code);
                	if(code == 0){
                		window.location.href = '${ctx}admin';
                	}else {
                		$("#captcha").val("");
               			$("#Verify_codeImag").attr("src","${ctx}kaptcha.jpg?time=" + Math.random());
                		if(code == 1 || code == 2) {
                			resetInput();
                			showTopMsg("账号或密码错误", 4000, 'error');
                		} else if(code == 4){
                			$("#captcha").focus();
                			showTopMsg("验证码错误", 4000, 'error');
                		} else {
                			resetInput();
                			showTopMsg("服务器连接不上,联系管理员！", 4000, 'error');
                		}
                        CheckingLogin(0);
                        return false;
                    } 
                });
            }
        }
        //清空
        function resetInput() {
            $("#username").focus(); //默认焦点
            $("#username").val("");
            $("#password").val("");
            $("#captcha").val("");
        }
        function CheckingLogin(id) {
            if (id == 1) {
                $("#Log_Submit").attr("disabled", "disabled")
                $("#Log_Submit").attr("class", "signload");
                $(".load").show();
            } else {
                $("#Log_Submit").removeAttr('disabled');
                $("#Log_Submit").attr("class", "sign");
                $(".load").hide();
            }
        }
    </script>
</head>
<body>
    <div id="thead">
        <div class="bg1"></div>
        <div class="bg2"></div>
        <div class="bg3">
            <div class="nav">
                <ul class="Form FancyForm">
                    <li>
                        <input id="username" name="username" type="text" class="stext" value="admin" placeholder="登陆账号">
                    </li>
                    <li>
                        <input id="password" name="password" type="password" class="stext" value="123456" placeholder="密码">
                    </li>
                    <li>
                        <input id="captcha" name="captcha" type="text" class="stext" maxlength="4"  style="width: 100px; margin-top:1ime-mode: disabled" placeholder="验证码">
                    </li>
                </ul>
                <div class="s8">
                    <table>
                        <tbody><tr style="height: 40px;">
                            <td>
                                <img src="${ctx }kaptcha.jpg" onclick="javascript:this.src='${ctx}kaptcha.jpg'" id="Verify_codeImag" width="80" height="35" alt="点击切换验证码" title="点击切换验证码" style="cursor: pointer;">
                            </td>
                            <td>
                                <input id="Log_Submit" type="button" class="sign" onclick="return CheckUserDataValid();">
                            </td>
                        </tr>
                    </tbody></table>
                </div>
                <div class="load">
                    <img src="${ctx }static/images/Login/loading.gif">
                </div>
            </div>
        </div>
        <div class="bg4">
            <p>
                适用浏览器：IE8、360、FireFox、Chrome、Safari、Opera、傲游、搜狗、世界之窗。
            </p>
        </div>
    </div>
    <div id="bottom">
    </div>


</body></html>