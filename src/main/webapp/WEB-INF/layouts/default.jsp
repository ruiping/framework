<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="ctx" value="${pageContext.request.contextPath}/" />

<!DOCTYPE html>
<html>
<head>
<title>小说站管理系统:<sitemesh:title/></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

<link rel="stylesheet" href="${ctx }static/css/style.default.css" type="text/css" />
<script type="text/javascript" src="${ctx }static/js/plugins/jquery-1.7.min.js"></script>
<script type="text/javascript" src="${ctx }static/js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="${ctx }static/js/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx }static/js/plugins/jquery.uniform.min.js"></script>
<script type="text/javascript" src="${ctx }static/js/plugins/jquery.flot.min.js"></script>
<script type="text/javascript" src="${ctx }static/js/plugins/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="${ctx }static/js/plugins/jquery.slimscroll.js"></script>
<script type="text/javascript" src="${ctx }static/js/custom/general.js"></script>
<sitemesh:head/>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="${ctx }static/js/plugins/excanvas.min.js"></script><![endif]-->
<!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="${ctx }static/css/style.ie9.css"/>
<![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="${ctx }static/css/style.ie8.css"/>
<![endif]-->
<!--[if lt IE 9]>
	<script src="${ctx }static/js/plugins/css3-mediaqueries.js"></script>
<![endif]-->
</head>

<body class="withvernav">
<div class="bodywrapper">
	<%@ include file="/WEB-INF/layouts/header.jsp"%>
	<%@ include file="/WEB-INF/layouts/menu.jsp"%>
	<sitemesh:body/>
</div>
</body>
</html>