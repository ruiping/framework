package com.framework.entity.admin.system;

import com.framework.common.Page;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Page page;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Page getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }
        
        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }
        
        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }	
        
        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }
        
        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        public Criteria andCodeIsNull() {
            addCriterion("code is null");
            return (Criteria) this;
        }
        
        public Criteria andCodeIsNotNull() {
            addCriterion("code is not null");
            return (Criteria) this;
        }
        
        public Criteria andCodeEqualTo(String value) {
            addCriterion("code =", value, "code");
            return (Criteria) this;
        }	
        
        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("code <>", value, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeGreaterThan(String value) {
            addCriterion("code >", value, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("code >=", value, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeLessThan(String value) {
            addCriterion("code <", value, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("code <=", value, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeIn(List<String> values) {
            addCriterion("code in", values, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("code not in", values, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("code between", value1, value2, "code");
            return (Criteria) this;
        }
        
        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("code not between", value1, value2, "code");
            return (Criteria) this;
        }
        public Criteria andUserNameIsNull() {
            addCriterion("userName is null");
            return (Criteria) this;
        }
        
        public Criteria andUserNameIsNotNull() {
            addCriterion("userName is not null");
            return (Criteria) this;
        }
        
        public Criteria andUserNameEqualTo(String value) {
            addCriterion("userName =", value, "userName");
            return (Criteria) this;
        }	
        
        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("userName <>", value, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("userName >", value, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("userName >=", value, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameLessThan(String value) {
            addCriterion("userName <", value, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("userName <=", value, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameIn(List<String> values) {
            addCriterion("userName in", values, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("userName not in", values, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("userName between", value1, value2, "userName");
            return (Criteria) this;
        }
        
        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("userName not between", value1, value2, "userName");
            return (Criteria) this;
        }
        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }
        
        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }
        
        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }	
        
        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }
        
        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }
        public Criteria andRealNameIsNull() {
            addCriterion("realName is null");
            return (Criteria) this;
        }
        
        public Criteria andRealNameIsNotNull() {
            addCriterion("realName is not null");
            return (Criteria) this;
        }
        
        public Criteria andRealNameEqualTo(String value) {
            addCriterion("realName =", value, "realName");
            return (Criteria) this;
        }	
        
        public Criteria andRealNameNotEqualTo(String value) {
            addCriterion("realName <>", value, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameGreaterThan(String value) {
            addCriterion("realName >", value, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameGreaterThanOrEqualTo(String value) {
            addCriterion("realName >=", value, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameLessThan(String value) {
            addCriterion("realName <", value, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameLessThanOrEqualTo(String value) {
            addCriterion("realName <=", value, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameIn(List<String> values) {
            addCriterion("realName in", values, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameNotIn(List<String> values) {
            addCriterion("realName not in", values, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameBetween(String value1, String value2) {
            addCriterion("realName between", value1, value2, "realName");
            return (Criteria) this;
        }
        
        public Criteria andRealNameNotBetween(String value1, String value2) {
            addCriterion("realName not between", value1, value2, "realName");
            return (Criteria) this;
        }
        public Criteria andSaltIsNull() {
            addCriterion("salt is null");
            return (Criteria) this;
        }
        
        public Criteria andSaltIsNotNull() {
            addCriterion("salt is not null");
            return (Criteria) this;
        }
        
        public Criteria andSaltEqualTo(String value) {
            addCriterion("salt =", value, "salt");
            return (Criteria) this;
        }	
        
        public Criteria andSaltNotEqualTo(String value) {
            addCriterion("salt <>", value, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltGreaterThan(String value) {
            addCriterion("salt >", value, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltGreaterThanOrEqualTo(String value) {
            addCriterion("salt >=", value, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltLessThan(String value) {
            addCriterion("salt <", value, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltLessThanOrEqualTo(String value) {
            addCriterion("salt <=", value, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltIn(List<String> values) {
            addCriterion("salt in", values, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltNotIn(List<String> values) {
            addCriterion("salt not in", values, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltBetween(String value1, String value2) {
            addCriterion("salt between", value1, value2, "salt");
            return (Criteria) this;
        }
        
        public Criteria andSaltNotBetween(String value1, String value2) {
            addCriterion("salt not between", value1, value2, "salt");
            return (Criteria) this;
        }
        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }
        
        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }
        
        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }	
        
        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }
        
        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }
        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }
        
        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }
        
        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }	
        
        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }
        
        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }
        public Criteria andTelephoneIsNull() {
            addCriterion("telephone is null");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneIsNotNull() {
            addCriterion("telephone is not null");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneEqualTo(String value) {
            addCriterion("telephone =", value, "telephone");
            return (Criteria) this;
        }	
        
        public Criteria andTelephoneNotEqualTo(String value) {
            addCriterion("telephone <>", value, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneGreaterThan(String value) {
            addCriterion("telephone >", value, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("telephone >=", value, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneLessThan(String value) {
            addCriterion("telephone <", value, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneLessThanOrEqualTo(String value) {
            addCriterion("telephone <=", value, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneIn(List<String> values) {
            addCriterion("telephone in", values, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneNotIn(List<String> values) {
            addCriterion("telephone not in", values, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneBetween(String value1, String value2) {
            addCriterion("telephone between", value1, value2, "telephone");
            return (Criteria) this;
        }
        
        public Criteria andTelephoneNotBetween(String value1, String value2) {
            addCriterion("telephone not between", value1, value2, "telephone");
            return (Criteria) this;
        }
        public Criteria andBirthdayIsNull() {
            addCriterion("birthday is null");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayIsNotNull() {
            addCriterion("birthday is not null");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayEqualTo(Date value) {
            addCriterion("birthday =", value, "birthday");
            return (Criteria) this;
        }	
        
        public Criteria andBirthdayNotEqualTo(Date value) {
            addCriterion("birthday <>", value, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayGreaterThan(Date value) {
            addCriterion("birthday >", value, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayGreaterThanOrEqualTo(Date value) {
            addCriterion("birthday >=", value, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayLessThan(Date value) {
            addCriterion("birthday <", value, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayLessThanOrEqualTo(Date value) {
            addCriterion("birthday <=", value, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayIn(List<Date> values) {
            addCriterion("birthday in", values, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayNotIn(List<Date> values) {
            addCriterion("birthday not in", values, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayBetween(Date value1, Date value2) {
            addCriterion("birthday between", value1, value2, "birthday");
            return (Criteria) this;
        }
        
        public Criteria andBirthdayNotBetween(Date value1, Date value2) {
            addCriterion("birthday not between", value1, value2, "birthday");
            return (Criteria) this;
        }
        public Criteria andGenderIsNull() {
            addCriterion("gender is null");
            return (Criteria) this;
        }
        
        public Criteria andGenderIsNotNull() {
            addCriterion("gender is not null");
            return (Criteria) this;
        }
        
        public Criteria andGenderEqualTo(Boolean value) {
            addCriterion("gender =", value, "gender");
            return (Criteria) this;
        }	
        
        public Criteria andGenderNotEqualTo(Boolean value) {
            addCriterion("gender <>", value, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderGreaterThan(Boolean value) {
            addCriterion("gender >", value, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderGreaterThanOrEqualTo(Boolean value) {
            addCriterion("gender >=", value, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderLessThan(Boolean value) {
            addCriterion("gender <", value, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderLessThanOrEqualTo(Boolean value) {
            addCriterion("gender <=", value, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderIn(List<Boolean> values) {
            addCriterion("gender in", values, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderNotIn(List<Boolean> values) {
            addCriterion("gender not in", values, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderBetween(Boolean value1, Boolean value2) {
            addCriterion("gender between", value1, value2, "gender");
            return (Criteria) this;
        }
        
        public Criteria andGenderNotBetween(Boolean value1, Boolean value2) {
            addCriterion("gender not between", value1, value2, "gender");
            return (Criteria) this;
        }
        public Criteria andLogOnCountIsNull() {
            addCriterion("logOnCount is null");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountIsNotNull() {
            addCriterion("logOnCount is not null");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountEqualTo(Integer value) {
            addCriterion("logOnCount =", value, "logOnCount");
            return (Criteria) this;
        }	
        
        public Criteria andLogOnCountNotEqualTo(Integer value) {
            addCriterion("logOnCount <>", value, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountGreaterThan(Integer value) {
            addCriterion("logOnCount >", value, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("logOnCount >=", value, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountLessThan(Integer value) {
            addCriterion("logOnCount <", value, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountLessThanOrEqualTo(Integer value) {
            addCriterion("logOnCount <=", value, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountIn(List<Integer> values) {
            addCriterion("logOnCount in", values, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountNotIn(List<Integer> values) {
            addCriterion("logOnCount not in", values, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountBetween(Integer value1, Integer value2) {
            addCriterion("logOnCount between", value1, value2, "logOnCount");
            return (Criteria) this;
        }
        
        public Criteria andLogOnCountNotBetween(Integer value1, Integer value2) {
            addCriterion("logOnCount not between", value1, value2, "logOnCount");
            return (Criteria) this;
        }
        public Criteria andIpAddressIsNull() {
            addCriterion("ipAddress is null");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressIsNotNull() {
            addCriterion("ipAddress is not null");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressEqualTo(String value) {
            addCriterion("ipAddress =", value, "ipAddress");
            return (Criteria) this;
        }	
        
        public Criteria andIpAddressNotEqualTo(String value) {
            addCriterion("ipAddress <>", value, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressGreaterThan(String value) {
            addCriterion("ipAddress >", value, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressGreaterThanOrEqualTo(String value) {
            addCriterion("ipAddress >=", value, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressLessThan(String value) {
            addCriterion("ipAddress <", value, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressLessThanOrEqualTo(String value) {
            addCriterion("ipAddress <=", value, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressIn(List<String> values) {
            addCriterion("ipAddress in", values, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressNotIn(List<String> values) {
            addCriterion("ipAddress not in", values, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressBetween(String value1, String value2) {
            addCriterion("ipAddress between", value1, value2, "ipAddress");
            return (Criteria) this;
        }
        
        public Criteria andIpAddressNotBetween(String value1, String value2) {
            addCriterion("ipAddress not between", value1, value2, "ipAddress");
            return (Criteria) this;
        }
        public Criteria andMacAddressIsNull() {
            addCriterion("macAddress is null");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressIsNotNull() {
            addCriterion("macAddress is not null");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressEqualTo(String value) {
            addCriterion("macAddress =", value, "macAddress");
            return (Criteria) this;
        }	
        
        public Criteria andMacAddressNotEqualTo(String value) {
            addCriterion("macAddress <>", value, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressGreaterThan(String value) {
            addCriterion("macAddress >", value, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressGreaterThanOrEqualTo(String value) {
            addCriterion("macAddress >=", value, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressLessThan(String value) {
            addCriterion("macAddress <", value, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressLessThanOrEqualTo(String value) {
            addCriterion("macAddress <=", value, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressIn(List<String> values) {
            addCriterion("macAddress in", values, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressNotIn(List<String> values) {
            addCriterion("macAddress not in", values, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressBetween(String value1, String value2) {
            addCriterion("macAddress between", value1, value2, "macAddress");
            return (Criteria) this;
        }
        
        public Criteria andMacAddressNotBetween(String value1, String value2) {
            addCriterion("macAddress not between", value1, value2, "macAddress");
            return (Criteria) this;
        }
        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }	
        
        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }
        
        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }
        public Criteria andPreviousVisitIsNull() {
            addCriterion("previousVisit is null");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitIsNotNull() {
            addCriterion("previousVisit is not null");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitEqualTo(Date value) {
            addCriterion("previousVisit =", value, "previousVisit");
            return (Criteria) this;
        }	
        
        public Criteria andPreviousVisitNotEqualTo(Date value) {
            addCriterion("previousVisit <>", value, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitGreaterThan(Date value) {
            addCriterion("previousVisit >", value, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitGreaterThanOrEqualTo(Date value) {
            addCriterion("previousVisit >=", value, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitLessThan(Date value) {
            addCriterion("previousVisit <", value, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitLessThanOrEqualTo(Date value) {
            addCriterion("previousVisit <=", value, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitIn(List<Date> values) {
            addCriterion("previousVisit in", values, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitNotIn(List<Date> values) {
            addCriterion("previousVisit not in", values, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitBetween(Date value1, Date value2) {
            addCriterion("previousVisit between", value1, value2, "previousVisit");
            return (Criteria) this;
        }
        
        public Criteria andPreviousVisitNotBetween(Date value1, Date value2) {
            addCriterion("previousVisit not between", value1, value2, "previousVisit");
            return (Criteria) this;
        }
        public Criteria andLastVisitIsNull() {
            addCriterion("lastVisit is null");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitIsNotNull() {
            addCriterion("lastVisit is not null");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitEqualTo(Date value) {
            addCriterion("lastVisit =", value, "lastVisit");
            return (Criteria) this;
        }	
        
        public Criteria andLastVisitNotEqualTo(Date value) {
            addCriterion("lastVisit <>", value, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitGreaterThan(Date value) {
            addCriterion("lastVisit >", value, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitGreaterThanOrEqualTo(Date value) {
            addCriterion("lastVisit >=", value, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitLessThan(Date value) {
            addCriterion("lastVisit <", value, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitLessThanOrEqualTo(Date value) {
            addCriterion("lastVisit <=", value, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitIn(List<Date> values) {
            addCriterion("lastVisit in", values, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitNotIn(List<Date> values) {
            addCriterion("lastVisit not in", values, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitBetween(Date value1, Date value2) {
            addCriterion("lastVisit between", value1, value2, "lastVisit");
            return (Criteria) this;
        }
        
        public Criteria andLastVisitNotBetween(Date value1, Date value2) {
            addCriterion("lastVisit not between", value1, value2, "lastVisit");
            return (Criteria) this;
        }
        public Criteria andDepartmentIdIsNull() {
            addCriterion("departmentId is null");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdIsNotNull() {
            addCriterion("departmentId is not null");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdEqualTo(Integer value) {
            addCriterion("departmentId =", value, "departmentId");
            return (Criteria) this;
        }	
        
        public Criteria andDepartmentIdNotEqualTo(Integer value) {
            addCriterion("departmentId <>", value, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdGreaterThan(Integer value) {
            addCriterion("departmentId >", value, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("departmentId >=", value, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdLessThan(Integer value) {
            addCriterion("departmentId <", value, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdLessThanOrEqualTo(Integer value) {
            addCriterion("departmentId <=", value, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdIn(List<Integer> values) {
            addCriterion("departmentId in", values, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdNotIn(List<Integer> values) {
            addCriterion("departmentId not in", values, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdBetween(Integer value1, Integer value2) {
            addCriterion("departmentId between", value1, value2, "departmentId");
            return (Criteria) this;
        }
        
        public Criteria andDepartmentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("departmentId not between", value1, value2, "departmentId");
            return (Criteria) this;
        }
        public Criteria andEnabledIsNull() {
            addCriterion("enabled is null");
            return (Criteria) this;
        }
        
        public Criteria andEnabledIsNotNull() {
            addCriterion("enabled is not null");
            return (Criteria) this;
        }
        
        public Criteria andEnabledEqualTo(Boolean value) {
            addCriterion("enabled =", value, "enabled");
            return (Criteria) this;
        }	
        
        public Criteria andEnabledNotEqualTo(Boolean value) {
            addCriterion("enabled <>", value, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledGreaterThan(Boolean value) {
            addCriterion("enabled >", value, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledGreaterThanOrEqualTo(Boolean value) {
            addCriterion("enabled >=", value, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledLessThan(Boolean value) {
            addCriterion("enabled <", value, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledLessThanOrEqualTo(Boolean value) {
            addCriterion("enabled <=", value, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledIn(List<Boolean> values) {
            addCriterion("enabled in", values, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledNotIn(List<Boolean> values) {
            addCriterion("enabled not in", values, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledBetween(Boolean value1, Boolean value2) {
            addCriterion("enabled between", value1, value2, "enabled");
            return (Criteria) this;
        }
        
        public Criteria andEnabledNotBetween(Boolean value1, Boolean value2) {
            addCriterion("enabled not between", value1, value2, "enabled");
            return (Criteria) this;
        }
        public Criteria andDeleteMarkIsNull() {
            addCriterion("deleteMark is null");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkIsNotNull() {
            addCriterion("deleteMark is not null");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkEqualTo(Boolean value) {
            addCriterion("deleteMark =", value, "deleteMark");
            return (Criteria) this;
        }	
        
        public Criteria andDeleteMarkNotEqualTo(Boolean value) {
            addCriterion("deleteMark <>", value, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkGreaterThan(Boolean value) {
            addCriterion("deleteMark >", value, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkGreaterThanOrEqualTo(Boolean value) {
            addCriterion("deleteMark >=", value, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkLessThan(Boolean value) {
            addCriterion("deleteMark <", value, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkLessThanOrEqualTo(Boolean value) {
            addCriterion("deleteMark <=", value, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkIn(List<Boolean> values) {
            addCriterion("deleteMark in", values, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkNotIn(List<Boolean> values) {
            addCriterion("deleteMark not in", values, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkBetween(Boolean value1, Boolean value2) {
            addCriterion("deleteMark between", value1, value2, "deleteMark");
            return (Criteria) this;
        }
        
        public Criteria andDeleteMarkNotBetween(Boolean value1, Boolean value2) {
            addCriterion("deleteMark not between", value1, value2, "deleteMark");
            return (Criteria) this;
        }
        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }
        
        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }
        
        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }	
        
        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }
        
        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }
        public Criteria andCreateUserIdIsNull() {
            addCriterion("createUserId is null");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdIsNotNull() {
            addCriterion("createUserId is not null");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdEqualTo(Integer value) {
            addCriterion("createUserId =", value, "createUserId");
            return (Criteria) this;
        }	
        
        public Criteria andCreateUserIdNotEqualTo(Integer value) {
            addCriterion("createUserId <>", value, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdGreaterThan(Integer value) {
            addCriterion("createUserId >", value, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("createUserId >=", value, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdLessThan(Integer value) {
            addCriterion("createUserId <", value, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("createUserId <=", value, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdIn(List<Integer> values) {
            addCriterion("createUserId in", values, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdNotIn(List<Integer> values) {
            addCriterion("createUserId not in", values, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdBetween(Integer value1, Integer value2) {
            addCriterion("createUserId between", value1, value2, "createUserId");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("createUserId not between", value1, value2, "createUserId");
            return (Criteria) this;
        }
        public Criteria andCreateUserNameIsNull() {
            addCriterion("createUserName is null");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameIsNotNull() {
            addCriterion("createUserName is not null");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameEqualTo(String value) {
            addCriterion("createUserName =", value, "createUserName");
            return (Criteria) this;
        }	
        
        public Criteria andCreateUserNameNotEqualTo(String value) {
            addCriterion("createUserName <>", value, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameGreaterThan(String value) {
            addCriterion("createUserName >", value, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("createUserName >=", value, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameLessThan(String value) {
            addCriterion("createUserName <", value, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameLessThanOrEqualTo(String value) {
            addCriterion("createUserName <=", value, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameIn(List<String> values) {
            addCriterion("createUserName in", values, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameNotIn(List<String> values) {
            addCriterion("createUserName not in", values, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameBetween(String value1, String value2) {
            addCriterion("createUserName between", value1, value2, "createUserName");
            return (Criteria) this;
        }
        
        public Criteria andCreateUserNameNotBetween(String value1, String value2) {
            addCriterion("createUserName not between", value1, value2, "createUserName");
            return (Criteria) this;
        }
        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }	
        
        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }
        
        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }
        public Criteria andModifyUserIdIsNull() {
            addCriterion("modifyUserId is null");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdIsNotNull() {
            addCriterion("modifyUserId is not null");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdEqualTo(Integer value) {
            addCriterion("modifyUserId =", value, "modifyUserId");
            return (Criteria) this;
        }	
        
        public Criteria andModifyUserIdNotEqualTo(Integer value) {
            addCriterion("modifyUserId <>", value, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdGreaterThan(Integer value) {
            addCriterion("modifyUserId >", value, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("modifyUserId >=", value, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdLessThan(Integer value) {
            addCriterion("modifyUserId <", value, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("modifyUserId <=", value, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdIn(List<Integer> values) {
            addCriterion("modifyUserId in", values, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdNotIn(List<Integer> values) {
            addCriterion("modifyUserId not in", values, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdBetween(Integer value1, Integer value2) {
            addCriterion("modifyUserId between", value1, value2, "modifyUserId");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("modifyUserId not between", value1, value2, "modifyUserId");
            return (Criteria) this;
        }
        public Criteria andModifyUserNameIsNull() {
            addCriterion("modifyUserName is null");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameIsNotNull() {
            addCriterion("modifyUserName is not null");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameEqualTo(String value) {
            addCriterion("modifyUserName =", value, "modifyUserName");
            return (Criteria) this;
        }	
        
        public Criteria andModifyUserNameNotEqualTo(String value) {
            addCriterion("modifyUserName <>", value, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameGreaterThan(String value) {
            addCriterion("modifyUserName >", value, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("modifyUserName >=", value, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameLessThan(String value) {
            addCriterion("modifyUserName <", value, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameLessThanOrEqualTo(String value) {
            addCriterion("modifyUserName <=", value, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameIn(List<String> values) {
            addCriterion("modifyUserName in", values, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameNotIn(List<String> values) {
            addCriterion("modifyUserName not in", values, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameBetween(String value1, String value2) {
            addCriterion("modifyUserName between", value1, value2, "modifyUserName");
            return (Criteria) this;
        }
        
        public Criteria andModifyUserNameNotBetween(String value1, String value2) {
            addCriterion("modifyUserName not between", value1, value2, "modifyUserName");
            return (Criteria) this;
        }
        public Criteria andModifyTimeIsNull() {
            addCriterion("modifyTime is null");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modifyTime is not null");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modifyTime =", value, "modifyTime");
            return (Criteria) this;
        }	
        
        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modifyTime <>", value, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modifyTime >", value, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modifyTime >=", value, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modifyTime <", value, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modifyTime <=", value, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modifyTime in", values, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modifyTime not in", values, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modifyTime between", value1, value2, "modifyTime");
            return (Criteria) this;
        }
        
        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modifyTime not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}