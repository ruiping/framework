package com.framework.entity.admin.system;

import java.util.Date;

import com.framework.common.BaseEntity;

public class User extends BaseEntity {

	// 主键
	private Integer id;
	// 编码
	private String code;
	// 用户名
	private String userName;
	// 密码
	private String password;
	// 真实姓名
	private String realName;
	// 加密盐值
	private String salt;
	// 邮箱
	private String email;
	// 手机号码
	private String mobile;
	// 电话号码
	private String telephone;
	// 生日
	private Date birthday;
	// 性别
	private Boolean gender;
	// 登陆次数
	private Integer logOnCount;
	// ip地址
	private String ipAddress;
	// mac地址
	private String macAddress;
	// 描述
	private String description;
	// 上一次访问时间
	private Date previousVisit;
	// 最后访问时间
	private Date lastVisit;
	// 所属部门id
	private Integer departmentId;

	/* 辅助字段 start */
	private String plainPassword;

	/* 辅助字段 end */

	/**
	 * @param 设置主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return 返回主键
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param 设置编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return 返回编码
	 */
	public String getCode() {
		return this.code = code == null ? null : code.trim();
	}

	/**
	 * @param 设置用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return 返回用户名
	 */
	public String getUserName() {
		return this.userName = userName == null ? null : userName.trim();
	}

	/**
	 * @param 设置密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return 返回密码
	 */
	public String getPassword() {
		return this.password = password == null ? null : password.trim();
	}

	/**
	 * @param 设置真实姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * @return 返回真实姓名
	 */
	public String getRealName() {
		return this.realName = realName == null ? null : realName.trim();
	}

	/**
	 * @param 设置加密盐值
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @return 返回加密盐值
	 */
	public String getSalt() {
		return this.salt = salt == null ? null : salt.trim();
	}

	/**
	 * @param 设置邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return 返回邮箱
	 */
	public String getEmail() {
		return this.email = email == null ? null : email.trim();
	}

	/**
	 * @param 设置手机号码
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return 返回手机号码
	 */
	public String getMobile() {
		return this.mobile = mobile == null ? null : mobile.trim();
	}

	/**
	 * @param 设置电话号码
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return 返回电话号码
	 */
	public String getTelephone() {
		return this.telephone = telephone == null ? null : telephone.trim();
	}

	/**
	 * @param 设置生日
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return 返回生日
	 */
	public Date getBirthday() {
		return this.birthday;
	}

	/**
	 * @param 设置性别
	 */
	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	/**
	 * @return 返回性别
	 */
	public Boolean getGender() {
		return this.gender;
	}

	/**
	 * @param 设置登陆次数
	 */
	public void setLogOnCount(Integer logOnCount) {
		this.logOnCount = logOnCount;
	}

	/**
	 * @return 返回登陆次数
	 */
	public Integer getLogOnCount() {
		return this.logOnCount;
	}

	/**
	 * @param 设置ip地址
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return 返回ip地址
	 */
	public String getIpAddress() {
		return this.ipAddress = ipAddress == null ? null : ipAddress.trim();
	}

	/**
	 * @param 设置mac地址
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	/**
	 * @return 返回mac地址
	 */
	public String getMacAddress() {
		return this.macAddress = macAddress == null ? null : macAddress.trim();
	}

	/**
	 * @param 设置描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return 返回描述
	 */
	public String getDescription() {
		return this.description = description == null ? null : description.trim();
	}

	/**
	 * @param 设置上一次访问时间
	 */
	public void setPreviousVisit(Date previousVisit) {
		this.previousVisit = previousVisit;
	}

	/**
	 * @return 返回上一次访问时间
	 */
	public Date getPreviousVisit() {
		return this.previousVisit;
	}

	/**
	 * @param 设置最后访问时间
	 */
	public void setLastVisit(Date lastVisit) {
		this.lastVisit = lastVisit;
	}

	/**
	 * @return 返回最后访问时间
	 */
	public Date getLastVisit() {
		return this.lastVisit;
	}

	/**
	 * @param 设置所属部门id
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return 返回所属部门id
	 */
	public Integer getDepartmentId() {
		return this.departmentId;
	}

	public String getPlainPassword() {
		return plainPassword;
	}

	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}

}