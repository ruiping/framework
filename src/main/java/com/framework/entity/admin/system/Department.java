package com.framework.entity.admin.system;

import com.framework.common.BaseEntity;
import java.util.Date;

public class Department extends BaseEntity{

	// 主键
    private Integer id;
	// 父级id
    private Integer parentId;
	// 编码
    private String code;
	// 名称
    private String fullName;
	// 简称
    private String shortName;
	// 联系电话
    private String phone;
	// 传真
    private String fax;
	// 邮箱
    private String email;
	// 描述，说明
    private String description;
	
	/**
	 * @param 设置主键
	 */
    public void setId(Integer id){
       	this.id = id;
    }
    /**
     * @return 返回主键 
     */
    public Integer getId(){
    	return this.id;	
    }
	/**
	 * @param 设置父级id
	 */
    public void setParentId(Integer parentId){
       	this.parentId = parentId;
    }
    /**
     * @return 返回父级id 
     */
    public Integer getParentId(){
    	return this.parentId;	
    }
	/**
	 * @param 设置编码
	 */
    public void setCode(String code){
       	this.code = code;
    }
    /**
     * @return 返回编码 
     */
    public String getCode(){
    	return this.code = code == null ? null : code.trim();
    }
	/**
	 * @param 设置名称
	 */
    public void setFullName(String fullName){
       	this.fullName = fullName;
    }
    /**
     * @return 返回名称 
     */
    public String getFullName(){
    	return this.fullName = fullName == null ? null : fullName.trim();
    }
	/**
	 * @param 设置简称
	 */
    public void setShortName(String shortName){
       	this.shortName = shortName;
    }
    /**
     * @return 返回简称 
     */
    public String getShortName(){
    	return this.shortName = shortName == null ? null : shortName.trim();
    }
	/**
	 * @param 设置联系电话
	 */
    public void setPhone(String phone){
       	this.phone = phone;
    }
    /**
     * @return 返回联系电话 
     */
    public String getPhone(){
    	return this.phone = phone == null ? null : phone.trim();
    }
	/**
	 * @param 设置传真
	 */
    public void setFax(String fax){
       	this.fax = fax;
    }
    /**
     * @return 返回传真 
     */
    public String getFax(){
    	return this.fax = fax == null ? null : fax.trim();
    }
	/**
	 * @param 设置邮箱
	 */
    public void setEmail(String email){
       	this.email = email;
    }
    /**
     * @return 返回邮箱 
     */
    public String getEmail(){
    	return this.email = email == null ? null : email.trim();
    }
	/**
	 * @param 设置描述，说明
	 */
    public void setDescription(String description){
       	this.description = description;
    }
    /**
     * @return 返回描述，说明 
     */
    public String getDescription(){
    	return this.description = description == null ? null : description.trim();
    }

}