package com.framework.vo;

/**
 * 查询过滤器
 * 
 * @Author: guitarpig
 * @Date: 2015年4月21日 上午10:43:34
 */
public class SearchFilter {

	public enum Condition {
		EMPTY, NOT_EMPTY, CONTAINS, CONTAINS_CASE_SENSITIVE, DOES_NOT_CONTAIN, DOES_NOT_CONTAIN_CASE_SENSITIVE, STARTS_WITH, STARTS_WITH_CASE_SENSITIVE, ENDS_WITH, ENDS_WITH_CASE_SENSITIVE, EQUAL, NOT_EQUAL, EQUAL_CASE_SENSITIVE, LESS_THAN, LESS_THAN_OR_EQUAL, GREATER_THAN, GREATER_THAN_OR_EQUAL;
	}

	// 字段名
	private String dataField;
	// 内容
	private String value;
	// 条件
	private Condition condition;

	public SearchFilter(String dataField, String value, Condition condition) {
		super();
		this.dataField = dataField;
		this.value = value;
		this.condition = condition;
	}

	public String getDataField() {
		return dataField;
	}

	public void setDataField(String dataField) {
		this.dataField = dataField;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

}
