package com.framework.vo;

import java.io.Serializable;

public interface IResult<T> extends Serializable {
	public T getData();

	public int getCode();

	public boolean success();

}