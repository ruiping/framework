package com.framework.vo;

/**
 * 查询参数
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月16日 下午3:28:10
 */
public class SearchParam {

	// 每页显示多少条数据
	private Integer rows;
	// 第几页
	private Integer page;

}
