package com.framework.vo;

/**
 * jqwidgets分页基础参数
 * 
 * @Author: guitarpig
 * @Date: 2015年4月27日 下午5:13:51
 */
public class JqxPaginationBaseParam {

	/** 动态参数 start */
	// filter参数名前缀
	public static final String FILTER_PREFIX = "filter";
	// filter内容参数名前缀
	public static final String FILTER_VALUE_PREFIX = "filtervalue";
	// filter条件参数名前缀
	public static final String FILTER_CONDITION_PREFIX = "filtercondition";
	// filter字段名参数前缀
	public static final String FILTER_DATEFIELD_PREFIX = "filterdatafield";
	/** 动态参数 end */

	// 排序字段
	private String sortdatafield;
	// 排序类型
	private String sortorder;
	// 当前第几页
	public int pagenum;
	// 每页多少条
	public int pagesize;
	// filter数量
	public int filterscount;

	public String getSortdatafield() {
		return sortdatafield;
	}

	public void setSortdatafield(String sortdatafield) {
		this.sortdatafield = sortdatafield;
	}

	public String getSortorder() {
		return sortorder;
	}

	public void setSortorder(String sortorder) {
		this.sortorder = sortorder;
	}

	public int getPagenum() {
		return pagenum;
	}

	public void setPagenum(int pagenum) {
		this.pagenum = pagenum;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public int getFilterscount() {
		return filterscount;
	}

	public void setFilterscount(int filterscount) {
		this.filterscount = filterscount;
	}

}
