package com.framework.util.generator.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 各类资源配置
 * 
 */
public class Resources {
	protected static Logger logger = LoggerFactory.getLogger(Resources.class);

	/************ mysql数据库配置 ************/
	public static String JDBC_DRIVER;
	public static String JDBC_URL;
	public static String JDBC_USERNAME;
	public static String JDBC_PASSWORD;

	public static String TPL_TABLE_NAME;// 表名
	public static String TPL_CLASS_NAME;// 类名，例：user
	public static String TPL_BASE_ENTITY;// 基础类完整包路径

	/************ 包名配置 ************/
	public static String PKN_ENTITY;// 实体包名
	public static String PKN_DAO;// dao层包名
	public static String PKN_SERVICE;// service层包名
	public static String PKN_CONTROLLER;// controller层包名

	static {
		Properties pops = new Properties();
		try {
			pops.load(Resources.class.getResourceAsStream("/generator/config/generator_settings.properties"));
			// JDBC
			JDBC_DRIVER = pops.getProperty("jdbc.driver");
			JDBC_URL = pops.getProperty("jdbc.url");
			JDBC_USERNAME = pops.getProperty("jdbc.username");
			JDBC_PASSWORD = pops.getProperty("jdbc.password");

			// TPL模板参数
			TPL_TABLE_NAME = pops.getProperty("tpl.table.name");
			TPL_CLASS_NAME = pops.getProperty("tpl.class.name");
			TPL_BASE_ENTITY = pops.getProperty("tpl.base.entity");

			PKN_ENTITY = pops.getProperty("pkn.entity");
			PKN_DAO = pops.getProperty("pkn.dao");
			PKN_SERVICE = pops.getProperty("pkn.service");
			PKN_CONTROLLER = pops.getProperty("pkn.controller");

			if (StringUtils.isBlank(PKN_ENTITY) || StringUtils.isBlank(PKN_DAO) || StringUtils.isBlank(PKN_SERVICE)
					|| StringUtils.isBlank(PKN_CONTROLLER) || StringUtils.isBlank(TPL_TABLE_NAME)
					|| StringUtils.isBlank(TPL_CLASS_NAME)) {
				logger.error("参数设置错误：表名、包名、类名。");
				System.exit(-1);
			}
			if (StringUtils.isNoneBlank(TPL_BASE_ENTITY)) {
				try {
					Class.forName(TPL_BASE_ENTITY);
				} catch (ClassNotFoundException e) {
					logger.error("参数设置错误-{}：找不到对应的父类。", "tpl.base.entity");
					System.exit(-1);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************ 模板配置 ************/
	public static final String TEMPLATE_PATH = "src/main/resources/generator/templates/";

	public static final String JAVA_ENTITY_TEMPLATE = "java_entity.ftl";
	public static final String JAVA_EXAMPLE_TEMPLATE = "java_example.ftl";
	public static final String JAVA_DAO_TEMPLATE = "java_dao.ftl";
	public static final String XML_DAO_TEMPLATE = "xml_dao.ftl";
	public static final String JAVA_SERVICE_TEMPLATE = "java_service.ftl";
	public static final String JAVA_CONTROLLER_TEMPLATE = "java_controller.ftl";

}
