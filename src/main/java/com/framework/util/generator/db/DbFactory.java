package com.framework.util.generator.db;

import com.framework.util.generator.util.Resources;

/**
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2014-7-24 下午1:55:20
 */
public class DbFactory {

	/**
	 * 创建数据库实例
	 * 
	 * @param dialect
	 *            mysql,sqlserver,db2,oracle
	 * @return
	 * @throws Exception
	 */
	public static DataSource create() throws Exception {
		DataSource db = null;
		String dialect = Resources.JDBC_DRIVER;
		if ((dialect == null) || "".equals(dialect)) {
			throw new Exception(DbFactory.class.getName() + ":>>>请指定数据库dialect......");
		}
		if ("com.mysql.jdbc.Driver".equals(dialect)) {
			db = new Mysql();
		}
		return db;
	}
}
