package com.framework.util.generator.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.framework.util.generator.util.Resources;

/**
 * 
 * 获取数据库连接
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2014-7-24 下午1:55:44
 */
public class DbConnection {

	static {
		try {
			Class.forName(Resources.JDBC_DRIVER).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static Connection getConn() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(Resources.JDBC_URL, Resources.JDBC_USERNAME, Resources.JDBC_PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

}
