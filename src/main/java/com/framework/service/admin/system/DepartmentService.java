package com.framework.service.admin.system;

import com.framework.entity.admin.system.Department;
import com.framework.entity.admin.system.DepartmentExample;
import com.framework.dao.admin.system.DepartmentDao;
import java.lang.Integer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.framework.common.BaseServiceSupport;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DepartmentService extends BaseServiceSupport<Department, DepartmentExample, DepartmentDao, Integer> {

	private static Logger logger = LoggerFactory.getLogger(DepartmentService.class);

	@Autowired
	private DepartmentDao departmentDao;
	
	@Override
	protected DepartmentDao getDao() {
		return departmentDao;
	}
}