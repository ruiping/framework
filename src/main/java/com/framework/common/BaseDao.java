package com.framework.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * 定义dao 基本方法
 * 
 * @param <D> 数据实体泛型
 * @param <E> 条件泛型
 * @param <PK> 主键泛型
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月9日 下午1:45:35
 */
public interface BaseDao<D, E, PK extends java.io.Serializable> {

	/**
	 * 按条件获得获得count
	 * 
	 * @param example
	 * @return
	 */
	int countByExample(E example);

	/**
	 * 按条件进行删除
	 * 
	 * @param example
	 * @return
	 */
	int deleteByExample(E example);

	/**
	 * 按主键删除
	 * 
	 * @param id
	 * @return
	 */
	int deleteByPrimaryKey(PK id);

	/**
	 * 新增数据
	 * 
	 * @param record
	 * @return
	 */
	int insert(D record);

	/**
	 * 新增数据 为空数据字段不插入
	 * 
	 * @param record
	 * @return
	 */
	int insertSelective(D record);

	/**
	 * 根据条件查询
	 * 
	 * @param example
	 * @return
	 */
	List<D> selectByExample(E example);

	/**
	 * 根据id 主键查询
	 * 
	 * @param id
	 * @return
	 */
	D selectByPrimaryKey(PK id);

	/**
	 * 按条件更新 为空数据字段不更新
	 * 
	 * @param record
	 * @param example
	 * @return
	 */
	int updateByExampleSelective(@Param("record") D record, @Param("example") E example);

	/**
	 * 按条件更新
	 * 
	 * @param record
	 * @param example
	 * @return
	 */
	int updateByExample(@Param("record") D record, @Param("example") E example);

	/**
	 * 按主键更新 为空数据字段不更新
	 * 
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeySelective(D record);

	/**
	 * 按主键更新
	 * 
	 * @param record
	 * @return
	 */
	int updateByPrimaryKey(D record);

}
