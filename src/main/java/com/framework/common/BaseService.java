package com.framework.common;

import java.util.List;

import com.framework.vo.IResult;

/**
 * 定义服务基本方法
 * 
 * @param <D> 数据库实体对象
 * @param <E> 查询条件
 * @param <PK> 主键类型
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月10日 上午10:40:06
 */
public interface BaseService<D, E, PK extends java.io.Serializable> {

	/**
	 * mysql 分页条件
	 */
	public final String LIMIT = " limit {0}, {1} ";

	/**
	 * 按条件获得获得count
	 * 
	 * @param example
	 * @return
	 */
	IResult<Integer> countByExample(E example);

	/**
	 * 按条件进行删除
	 * 
	 * @param example
	 * @return
	 */
	IResult<Integer> deleteByExample(E example);

	/**
	 * 新增数据
	 * 
	 * @param record
	 * @return
	 */
	IResult<Integer> save(D record);

	/**
	 * 新增数据 为空数据字段不插入
	 * 
	 * @param record
	 * @return
	 */
	IResult<Integer> saveSelective(D record);

	/**
	 * 新增更新实体对象 如果主键为null 去执行新增 否则执行更新
	 * 
	 * @param record
	 * @return
	 */
	IResult<Integer> saveOrUpdate(D record);

	/**
	 * 根据条件查询
	 * 
	 * @param example
	 * @return
	 */
	IResult<List<D>> findByExample(E example);

	/**
	 * 全部查询
	 * 
	 * @return
	 */
	IResult<List<D>> findAll();

}
