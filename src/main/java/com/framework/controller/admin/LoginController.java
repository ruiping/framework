package com.framework.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.framework.common.BaseController;
import com.framework.shiro.filter.CaptchaFormAuthenticationFilter;

/**
 * LoginController负责打开登录页面(GET请求)和登录出错页面(POST请求)，
 * 
 * 真正登录的POST请求由Filter完成,
 * 
 * @author calvin
 */
@Controller
@RequestMapping(value = "/admin")
public class LoginController extends BaseController {

	@RequestMapping(method = { RequestMethod.POST, RequestMethod.GET })
	public String index() {
		return "admin/index";
	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login() {
		if (getCurrentUser() != null) {
			return "redirect:/admin";
		}
		return "admin/login";
	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String fail(@RequestParam(CaptchaFormAuthenticationFilter.DEFAULT_USERNAME_PARAM) String userName,
			@RequestParam(CaptchaFormAuthenticationFilter.DEFAULT_PASSWORD_PARAM) String password,
			@RequestParam(CaptchaFormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME) String shiroLoginFailure,
			Model model) {
		model.addAttribute(CaptchaFormAuthenticationFilter.DEFAULT_USERNAME_PARAM, userName);
		model.addAttribute(CaptchaFormAuthenticationFilter.DEFAULT_PASSWORD_PARAM, password);
		return "admin/login";
	}

}
