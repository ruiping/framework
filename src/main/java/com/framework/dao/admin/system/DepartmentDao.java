package com.framework.dao.admin.system;

import com.framework.entity.admin.system.Department;
import com.framework.entity.admin.system.DepartmentExample;
import com.framework.common.BaseDao;
import com.framework.common.MyBatisRepository;

@MyBatisRepository
public interface DepartmentDao extends BaseDao<Department, DepartmentExample, Integer>{
   
}