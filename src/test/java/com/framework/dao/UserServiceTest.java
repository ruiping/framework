package com.framework.dao;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.framework.SpringTransactionalTestCase;
import com.framework.data.GenerateData;
import com.framework.entity.admin.system.User;
import com.framework.service.admin.system.UserService;
import com.framework.shiro.realm.ShiroDbRealm.ShiroUser;
import com.framework.util.ShiroTestUtils;

/**
 * AbstractTransactionalJUnit4SpringContextTests默认是自动回滚的
 * 测试service层的业务
 * 
 * @Author: lbl 543414648@qq.com
 * @Date: 2015年6月10日 上午11:26:03
 */
@ContextConfiguration(locations = { "/applicationContext.xml" })
@TransactionConfiguration(defaultRollback = false)
public class UserServiceTest extends SpringTransactionalTestCase {

	@Autowired
	private UserService userService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ShiroTestUtils.mockSubject(new ShiroUser(1, "admin", "Admin"));
	}

	@Test
	public void registerUser() {
		User user = GenerateData.randomNewUser();
		userService.save(user);
	}

}
